class MovieSearcher
  attr_reader :api_url, :api_key

  RESPONSE_KEY = 'Response'.freeze
  FALSE_VALUE = 'False'.freeze

  def initialize
    @api_key = '8379fcd9'
    @api_url = 'https://www.omdbapi.com/'
    # @api_key = ENV['OMDM_API_KEY']
    # @api_url = ENV['API_URL']
  end

  def search(name)
    response = Faraday.get(api_url, { t: name, apikey: api_key })
    response_json = JSON.parse(response.body)

    return nil if response_json[RESPONSE_KEY] == FALSE_VALUE

    Movie.new(response_json)
  end
end
