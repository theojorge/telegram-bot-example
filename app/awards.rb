class Awards
  attr_reader :info

  NO_VALUE = 'N/A'.freeze

  def initialize(info)
    @info = info
  end

  def response
    return 'Has not oscar awards or nominations' if info == NO_VALUE

    info
  end
end
