class Movie
  attr_reader :info

  TITLE_KEY = 'Title'.freeze
  AWARDS_VALUE = 'Awards'.freeze

  def initialize(info)
    @info = info
  end

  def awards
    awards = Awards.new(info[AWARDS_VALUE])
    awards.response
  end

  def name
    info[TITLE_KEY]
  end
end
