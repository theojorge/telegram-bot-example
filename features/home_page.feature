Feature: Response of movie oscar awards

  Scenario: Awards of a movie are asked
    When I am asked /awards titanic
    Then I should respond 'The movie titanic Won 11 Oscars. 126 wins & 83 nominations total'

  Scenario: Awards of a movie without awards are asked
    When I am asked /awards emoji
    Then I should respond 'The movie titanic Has not oscar awards or nominations'

  Scenario: Awards of a inexistent movie are asked
    When I am asked /awards aaaaaaa
    Then I should respond 'The movie aaaaaaa Does not exist'

  Scenario: Awards of two movie are asked
    When I am asked /awards titanic avatar
    Then I should respond 'The movie titanic Won 11 Oscars. 126 wins & 83 nominations total /n The movie avatar Won 3 Oscars. 91 wins & 131 nominations total /n'
