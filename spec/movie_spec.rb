require 'spec_helper'
require 'web_mock'
require_relative '../app/movie_searcher'

def when_i_contact_movie(movie, message)
  api_omdb_response_body = {
    "Title": movie,
    "Awards": message
  }

  stub_request(:get, "https://www.omdbapi.com/?apikey=8379fcd9&t=#{movie}")
    .with(
      headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: 200, body: api_omdb_response_body.to_json, headers: {})
end

describe 'MovieSearcher' do
  it 'should get a movie' do
    when_i_contact_movie('Titanic', 'Won 11 Oscars. 126 wins & 83 nominations total')
    searcher = MovieSearcher.new
    movie = searcher.search('Titanic')
    expect(movie.name).to eq('Titanic')
  end

  describe 'Movie' do
    it 'should get the awards of a movie' do
      when_i_contact_movie('Titanic', 'Won 11 Oscars. 126 wins & 83 nominations total')
      searcher = MovieSearcher.new
      movie = searcher.search('Titanic')
      expect(movie.awards).to eq('Won 11 Oscars. 126 wins & 83 nominations total')
    end

    it 'should get that the movie has not oscar awards and nominations' do
      when_i_contact_movie('Emoji', 'Has not oscar awards or nominations')
      searcher = MovieSearcher.new
      movie = searcher.search('Emoji')
      expect(movie.awards).to eq('Has not oscar awards or nominations')
    end

    it 'should get that the movie does not exist' do
      when_i_contact_movie('aaaaaaaaaaa', 'Does not exist')
      searcher = MovieSearcher.new
      movie = searcher.search('aaaaaaaaaaa')
      expect(movie.awards).to eq('Does not exist')
    end
  end
end
